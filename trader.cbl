       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TRADER.
       AUTHOR. SARAWUT PRAKOBKIJ 62160071.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT TRADER-MEMBER-FILE ASSIGN TO "trader1.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT TRADER-REPORT-FILE ASSIGN TO "trader.rpt"
              ORGANIZATION IS SEQUENTIAL.
              
       DATA DIVISION.
       FILE SECTION. 
       FD TRADER-REPORT-FILE.
       01 PRINT-LINE                    PIC X(44).
       FD TRADER-MEMBER-FILE.
       01 TRADER-REC.
          88 END-OF-TRADER-MEMBER-FILE                 VALUE HIGH-VALUES
           .
       05 TRADER-PROVINCE               PIC 9(2).
          05 TRADER-ID                  PIC 9(4).
          05 TRADER-INCOME              PIC 9(6).           

       WORKING-STORAGE SECTION. 

       01 PAGE-FOOTING.
          05 FILLER                     PIC X(14)      
           VALUE "MAX PROVINCE: ".
          05 MAX-PROVINCE PIC ZZZZ.
          05 FILLER                     PIC X(13)       
           VALUE " SUM INCOME: ".
          05 MAX-INCOME PIC $$$$,$$$,$$$.
       01 COLUMN-HEADING                PIC X(45)      VALUE 
      -     "PROVINCE   P INCOME    MEMBER  MEMBER INCOME".

       01 PRN-TRADER-DETAIL-LINE.
          05 FILLER                     PIC X(3)       VALUE SPACES.
          05 PRN-PROVINCE               PIC X(2).
          05 FILLER                     PIC X(5)       VALUE SPACES.
          05 PRN-PROVINCE-INCOME        PIC $9(3),9(3),9(3).
          05 FILLER                     PIC X(3)       VALUE SPACES.
          05 PRN-TRADER-MEMBER          PIC ZZZ9.
          05 FILLER                     PIC X(7)       VALUE SPACES.
          05 PRN-TRADER-MEMBER-INCOME   PIC $9(3),9(3).

       01 TRADER.
          05 PROVINCE                   PIC 9(2).
          05 PROVINCE-INCOME            PIC 9(9)       VALUE ZERO.
          05 MEMBER                     PIC 9(4).
          05 MEMBER-INCOME              PIC 9(6).

       01 HIGH-TRADER.
          05 HIGH-PROVINCE                   PIC 9(2).
          05 HIGH-PROVINCE-INCOME            PIC 9(9)       VALUE ZERO.
       
           


       PROCEDURE DIVISION.
       PROCESS-TRADER-REPORT.
           OPEN INPUT TRADER-MEMBER-FILE 
           OPEN OUTPUT TRADER-REPORT-FILE 
           PERFORM READ-TRADER-FILE

           PERFORM PROCESS-PAGE UNTIL END-OF-TRADER-MEMBER-FILE 

           CLOSE TRADER-REPORT-FILE 
           CLOSE TRADER-MEMBER-FILE
           GOBACK.

       PROCESS-PAGE.
           PERFORM WRITE-HEADING
           PERFORM PROCESS-DETAIL UNTIL END-OF-TRADER-MEMBER-FILE
           
           PERFORM WRITE-FOOTER
           
           .

       WRITE-FOOTER.
           DISPLAY PAGE-FOOTING
           WRITE PRINT-LINE FROM PAGE-FOOTING AFTER ADVANCING 1 LINE.


       PROCESS-DETAIL.
           
           IF NOT TRADER-PROVINCE EQUAL PROVINCE  
              IF NOT PROVINCE EQUAL 0
              MOVE PROVINCE TO PRN-PROVINCE
              MOVE PROVINCE-INCOME TO PRN-PROVINCE-INCOME
              MOVE MEMBER TO PRN-TRADER-MEMBER
              MOVE MEMBER-INCOME TO PRN-TRADER-MEMBER-INCOME
              DISPLAY PRN-TRADER-DETAIL-LINE 
              WRITE PRINT-LINE FROM PRN-TRADER-DETAIL-LINE
                    AFTER ADVANCING 1 LINE   
              IF PRN-PROVINCE-INCOME > MAX-INCOME 
                 MOVE PRN-PROVINCE TO MAX-PROVINCE 
                 MOVE PRN-PROVINCE-INCOME TO MAX-INCOME  
              END-IF
              END-IF

              
                 
              MOVE TRADER-PROVINCE TO PROVINCE 
              SET TRADER-INCOME TO PROVINCE-INCOME
              MOVE TRADER-ID TO MEMBER
              MOVE TRADER-INCOME TO MEMBER-INCOME

              
           ELSE
              ADD TRADER-INCOME TO PROVINCE-INCOME 
              IF TRADER-INCOME > MEMBER-INCOME 
                 MOVE TRADER-ID TO MEMBER
                 MOVE TRADER-INCOME TO MEMBER-INCOME
              END-IF
           END-IF
           
           PERFORM READ-TRADER-FILE
           
           .

       WRITE-HEADING.
           DISPLAY COLUMN-HEADING
           WRITE PRINT-LINE FROM COLUMN-HEADING
           .

       READ-TRADER-FILE.
           READ TRADER-MEMBER-FILE
           AT END
              SET END-OF-TRADER-MEMBER-FILE TO TRUE
           END-READ
           .